<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class UsersActionButton extends AbstractAction
{
    public function getTitle()
    {
        return 'Open';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-left',
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        // show or hide the action button, in this case will show for posts model
        return $this->dataType->slug == 'users';
    }

    public function getDefaultRoute()
    {
//        return route('users.wallet_configuration', $this->data->id);
//        return route('voyager.'.$this->dataType->slug.'.edit', $this->data->{$this->data->getKeyName()});
    }
}
