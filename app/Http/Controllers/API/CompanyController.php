<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyCollection;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    //
    public function getCompanies(Request $request)
    {
        $userId = Auth::user()->id;
        $companies = Company::where('created_by', $userId)->orderBy('name', 'asc')->get();

        return self::success('Companies', ['data' => $companies]);
    }

    public function store(Request $request)
    {

        $validators = Validator::make($request->all(), [
            'name' => 'required|string|max:255'
        ]);


        if ($validators->fails())
        {
            return self::failure($validators->errors()->first());
        }

        $userId = Auth::user()->id;

        $data = $request->all();

        $company = new Company();
        $company->name = $data['name'];
        $company->created_by = $userId;
        $company->save();

        $obj = [
            'company_id' => $company->id,
            'user_id' => $userId
        ];
        CompanyUser::updateOrCreate($obj, $obj);

        $company = new CompanyResource($company);

        return self::success('Created', ['data' => $company ]);
    }

    public function update(Request $request)
    {
        $validators = Validator::make($request->all(), [
            'id' => 'required|exists:companies,id',
            'name' => 'required|string|max:255'
        ]);


        if ($validators->fails())
        {
            return self::failure($validators->errors()->first());
        }

        $data = $request->all();
        $company = Company::where('id', $data['id'])->first();
        $company->update([
            'name' => $data['name']
        ]);
        $company = Company::where('id', $data['id'])->first();
        $company = new CompanyResource($company);
        return self::success('Updated',  ['data' => $company ]);

    }

    public function getCompanyById(Request $request, $id)
    {
        $userId = Auth::user()->id;
        $company = Company::where(['created_by' => $userId, 'id' => $id ])->first();
        if($company){
            $company = new CompanyResource($company);
            return self::success("Company", ['data' => $company ]);
        }
        return self::failure("Company not found");
    }


    public function deleteCompanyById(Request $request, $id) {

        $userId = Auth::user()->id;
        $company = Company::where(['created_by' => $userId, 'id' => $id ])->first();
        if($company){
            CompanyUser::where('company_id', $company->id)->delete();
            $company->delete();
            return self::success("Company Deleted", ['data' => null ]);
        }
        return self::failure("Company not found");
    }

    public function addUsersByCompanyId(Request $request, $company_id){

        $data = $request->data;

        $userId = Auth::user()->id;
        $company = Company::where(['created_by' => $userId, 'id' => $company_id ])->first();
        if(!$company){
            return self::failure("Company not found");
        }

        foreach ($data as $d){

            $user = User::where('email', $d['email'])->first();
            if(!$user){
                $user = new User($d);
                $user->save();
            }

            $obj = [
                'company_id' => $company->id,
                'user_id' => $user->id
            ];
            CompanyUser::updateOrCreate($obj, $obj);

        }

        return self::success("Users Added Successfully");

    }
}
