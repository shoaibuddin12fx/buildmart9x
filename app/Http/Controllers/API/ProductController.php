<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Voyager\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function fetchProducts(Request $request)
    {

        $data = $request->all();
        $q = $data['term'];
        $searchtype = $data['searchtype'];

        $query = Product::query();

        if($searchtype == 'barcode'){
            $query = $query->where('item_bar_code', 'like', "%$q%");
        }else{
            $query = $query->where('item_name', 'like', "%$q%");
        }

        $products = $query->limit(5)->get()->transform(function ($item, $key) use ($searchtype) {
            $obj = [
                "id" => $item->id,
                "text" => $searchtype == 'barcode' ? $item->item_bar_code : $item->item_name
            ];
            return $obj;
        });

        return self::success('products', ['items' => $products] );
    }

    public function generateProductCode(Request $request)
    {
        $barcode = 'BM-' . Carbon::now()->format('YmdHis');
        return self::success('Barcode', [ 'bar_code' => $barcode ]);
    }

    public function getProductById(Request $request)
    {
        $id = $request->input('id');
        $product = Product::where('id', $id)->first();
        return self::success('Product', [ 'product' => $product ]);
    }

}
