<?php
namespace App\Http\Controllers\API;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first()); //response(['errors'=>$validator->errors()->all()], 422);
        }

        $user = User::where('email', $request->email)->where('id', '!=', 1)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $user = new UserResource($user);
                $response = ['token' => $token, 'user' => $user ];
                return self::success("Login Successful", [ 'data' => $response ]);
            } else {
                return self::failure("Password mismatch");
            }
        } else {
            return self::failure('User does not exist');
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first());
        }

        $data = $request->all();

        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = Str::random(10);

        $user = new User($data);
        $user->role_id = 2;
        $user->save();

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $user = new UserResource($user);
        $response = ['token' => $token, 'user' => $user ];

        return self::success("Login Successful", ['data' => $response ] );

    }

    public function getProfile(Request $request){

        $user = \Auth()->user();
        if($user){
            $profile = User::where('id', $user->id)->first();
            if($profile){
                $profile = new UserResource($profile);
                return self::success("User Profile", [ 'data' => $profile ] );
            }else{
                return self::failure("No User(s) Exist" );
            }

        }else{
            return self::failure("No User(s) Exist" );
        }

    }

    public function updateProfile(Request $request){
        $data = $request->all();
        $user = \Auth()->user();
        if($user){
            $user->update($data);
            $profile = User::where('id', $user->id)->first();
            $profile = new UserResource($profile);
            return self::success("User Profile", [ 'data' => $profile ] );
        }else{
            return self::failure("No User(s) Exist" );
        }

    }
}
