<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {

        $user =  User::where('id', $obj->created_by )->first() ? new UserResource(User::where('id', $obj->created_by )->first()) : null;

        return [
            "id" => $obj->id,
            "name" => $obj->name,
            "created_by" => $obj->created_by,
            "user" => $user


        ];
    }
}
