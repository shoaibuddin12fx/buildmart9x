<?php

namespace App\Http\Resources;

use App\Models\Voyager\Subscription;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SubscriptionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Subscription $subscription){
            return new SubscriptionResource($subscription);
        });

        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {

        $obj->transform(function (Subscription $subscription){
            return  new SubscriptionResource($subscription);
        });

        return $obj;
    }
}
