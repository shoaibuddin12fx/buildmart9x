<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {
        return [
            "id" => $obj->id,
            "title" => $obj->title,
            "description" => $obj->description,
            "price" => $obj->price,
            "color" => $obj->color,
            "user_limit" => $obj->user_limit,
            "type" => $obj->type
        ];
    }
}
