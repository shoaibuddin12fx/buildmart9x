<?php

namespace App\Models\Voyager;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStoreLocation extends Model
{
    use HasFactory;
}
