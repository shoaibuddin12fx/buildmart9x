/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import VueRouter from "vue-router";
import Loading from 'vue-loading-overlay';
import VueMq from 'vue-mq';
import VueI18n from 'vue-i18n'
import SqliteService from "./services/sqlite.service";
import Vue from 'vue';
let en = require('../lang/en.json');
let ar = require('../lang/ar.json');
var messages = {
    "en": en,
    "ar": ar
} // require('./config/lang.json');
require('./bootstrap');

// import routes from "./routes";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


import {routes} from "./routes";

window.Vue = require('vue').default;
Vue.use(VueRouter);
Vue.use(VueI18n);
Vue.mixin(require('./asset'));

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)|
Vue.use(Loading, {
    isFullPage: true,
    loader: 'spinner'
});
Vue.use(VueMq, {
    breakpoints: {
        sm: 450,
        md: 1250,
        lg: Infinity,
    }
});



// Create VueI18n instance with options
const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('app-component', require('./views/App.vue').default);
require("./components")

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
    base: '/buildmart8x/public',
    routes: routes,
    mode: "history"
})

let sqlite = new SqliteService();

sqlite.initialize().then( v => {
    const app = new Vue({
        el: '#app',
        i18n,
        router
    });
})


