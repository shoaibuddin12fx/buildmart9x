
// Declare All the Components Here

Vue.component('top-header', require('./components/header/TopHeaderComponent').default);
Vue.component('main-menu', require('./components/header/MenuComponent/MenuComponent').default);
Vue.component('nav-search', require('./components/header/HeaderSearch/HeaderSearchComponent').default);
Vue.component('header-meta', require('./components/header/HeaderMeta/HeaderMetaComponent').default);
Vue.component('header-bar-categories', require('./components/header/HeaderBarCategories/HeaderBarCategories').default);
Vue.component('header-slider', require('./components/header/HeaderSlider/HeaderSlider').default);
Vue.component('main-footer', require('./components/footer/FooterComponent').default);
Vue.component('locale-changer', require('./components/header/MenuComponent/LocaleChanger').default);
Vue.component('sign-in-page', require('./pages/signin-page/SignInPageComponent').default);
Vue.component('why-us', require('./components/home/why-us/WhyUsComponent').default);
Vue.component('home-carousel', require('./components/home/home-page-carousel/HomePageCarouselComponent').default);
Vue.component('shop-by-category', require('./components/home/shop-by-category/ShopByCategoryComponent').default);
Vue.component('brands', require('./components/home/brands/BrandsComponent').default);
Vue.component('hot-offers', require('./components/home/hot-offers/HotOffersComponent').default);
Vue.component('home-categories', require('./components/home/categories/CategoriesComponent').default);
Vue.component('home-section-names', require('./components/home/section-names/SectionNameComponent').default);
Vue.component('home-banner', require('./components/home/home-banner/HomeBannerComponent').default);
Vue.component('coming-soon', require('./components/home/coming-soon/ComingSoonComponent').default);
Vue.component('subscription-packages', require('./pages/profile-page/subscription-packages/SubscriptionPackageComponent').default);
Vue.component('user-profile', require('./pages/profile-page/user-profile/UserProfileComponent').default);
Vue.component('header-second', require('./components/header/HeaderSecond/HeaderSecond').default )

// modals
Vue.component('login-modal', require('./components/modals/LoginModal/LoginModal').default);
