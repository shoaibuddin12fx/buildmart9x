
import SqliteService from "./services/sqlite.service";
let sqlite = new SqliteService();


async function authGuard(to, from, next)
{
    var isAuthenticated= false;
    //this is just an example. You will have to find a better or
    // centralised way to handle you localstorage data handling
    let user = await sqlite.getActiveUser();
    if(user){
        next(); // allow to enter route
    }else{
        next('/sign-in'); // go to '/login';
    }
}

async function inverseAuthGuard(to, from, next)
{
    var isAuthenticated= false;
    //this is just an example. You will have to find a better or
    // centralised way to handle you localstorage data handling
    let user = await sqlite.getActiveUser();
    if(!user){
        next(); // allow to enter route
    }else{
        next('/profile'); // go to '/login';
    }
}

export const routes =  [
    {path: '/', component: require('./pages/home-page/HomePageComponent').default},
    {path: '/sign-in', component: require('./pages/signin-page/SignInPageComponent').default, beforeEnter: inverseAuthGuard},
    {path: '/profile', component: require('./pages/profile-page/ProfilePageComponent').default, beforeEnter: authGuard},
];
