import SqliteService from "./sqlite.service";

var json = require('../../js/config/config.json');
const sqliteService = new SqliteService();



export default class ApiService {

    constructor() {
        json.base_url = 'api/'


    }

    get(api, config = {}) {
        console.log({api})
        return new Promise( resolve => {
            window.axios.get(json.base_url+api, config).then( res => {
                resolve(res.data);
            });
        })

    }

    post(api, data, config = {}) {
        return new Promise( resolve => {
            window.axios.post(json.base_url+api, data, config).then(res => {
                resolve(res.data);
            });
        })
    }

    // postLogin(api, data, config = {}) {
    //     return new Promise( resolve => {
    //         window.axios.post(json.base_url+api, data, config).then(res => {
    //             resolve(res.data);
    //         });
    //     })
    // }

    put(api, data, config = {}) {
        console.log({config})
        return window.axios.put(json.base_url+api, data, config).then(res => {
            resolve(res.data);
        });
    }
    delete(api, id, config = {}) {
        return Vue.axios.delete(json.base_url+api, id, );
    }




}
