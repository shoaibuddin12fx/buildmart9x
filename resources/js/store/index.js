import Vue from 'vue';
import Vuex from 'vuex';

// let sqlite3 = require('sqlite3').verbose();

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        database: null,
        data: []
    },
    mutations: {
        // init(state, data) {
        //     state.database = data.database;
        // },
        // load(state, data) {
        //     state.data = [];
        //     state.data.push({
        //         key: data.data[i][0],
        //     });
        // },
        // save(state, data) {
        //     state.data.push({
        //         key: data.data.key,
        //     });
        // },

        saveUserKey(state, data) {
            state.data.push({
                key: data,
            })
        }
    },
    getters: {
        doneTodos: state => {
            return state.data
        }
    },
    actions: {
        // init(context) {
        //     (new sqlite3.Database('database')).then(db => {
        //         db.execSQL("CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT)").then(id => {
        //             context.commit("init", {database: db});
        //         }, error => {
        //             console.log("CREATE TABLE ERROR", error);
        //         });
        //     }, error => {
        //         console.log("OPEN DB ERROR", error);
        //     });
        // },
        // insert(context, data) {
        //     context.state.database.execSQL("INSERT INTO user (key) VALUES (?)", [data.key]).then(id => {
        //         context.commit("save", {data: data});
        //     }, error => {
        //         console.log("INSERT ERROR", error);
        //     });
        // },
        // query(context) {
        //     context.state.database.all("SELECT key FROM user", []).then(result => {
        //         context.commit("load", {data: result});
        //     }, error => {
        //         console.log("SELECT ERROR", error);
        //     });
        // }

        setUser(context, data) {
            context.commit("saveUserKey", data.key);
        }
    }
});

Vue.prototype.$store = store;

export default store;

store.dispatch("init");
