<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .vld-overlay.is-active {
            position: absolute;
            left: 50%;
            top: 50%;
            right: 50%;
            bottom: 50%;
            width: 100%;
            height: 100%;
            z-index: 9999;
        }


    </style>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script defer src="{{ asset('js/app.js') }}" type="application/javascript"></script>
    <script>
        var laravel = @json([ 'baseURL' => url('/'), 'csrfToken' => csrf_token()  ])
    </script>

    <script>
        window._asset = '{{ asset('') }}';
    </script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@200;300;400;500;700;800;900&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div id="app" class="milage">

    <router-view></router-view>
    <div class="row">
        <div class="col-12" style="background-color: #F2F2F2;">
            <main-footer></main-footer>
        </div>
    </div>


</div>
</body>
</html>
